<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.04.15
 * Time: 14:14
 */

namespace Dicom\VitalImagesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OAuth2\ServerBundle\Entity\User;

/**
 * VitalImages
 */
class VitalImages
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $user_id;

    /**
     * @var \OAuth2\ServerBundle\Entity\User
     */
    protected $user;

    /**
     * Get Id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Login
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set Login
     * @param $login
     * @return string
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this->login;
    }

    /**
     * Get Password
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set Password
     * @param $password
     * @return string
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this->password;
    }

    /**
     * Get UserId
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set UserId
     * @param $userId
     * @return string
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this->user_id;
    }

    /**
     * Get User
     * @return \OAuth2\ServerBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        $this->user_id = $user->getId();

        return $this;
    }

    /**
     * Return a array representation of vitalImages
     * @return array key => value
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'login' => $this->getLogin(),
            'password' => $this->getPassword(),
            'userId' => $this->getUserId()
        ];
    }
}