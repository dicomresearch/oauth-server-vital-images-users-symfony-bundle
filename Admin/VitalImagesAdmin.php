<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 07.04.15
 * Time: 16:05
 */

namespace Dicom\VitalImagesBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class VitalImagesAdmin extends Admin
{
    // установка сортировки по умолчанию
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by'    => 'id'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', 'integer', ['disabled' => 'true', 'label' => $this->trans('ID')])
            ->add('login', 'text', ['label' => $this->trans('Login')])
            ->add('password', 'text', ['label' => $this->trans('Password')])
            ->add('user', 'sonata_type_model', [
                'label' => $this->trans('User'),
                'placeholder' => $this->trans('Choose user'),
                'btn_add' => false
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('login')
            ->add('password')
            ->add('user', null, ['field_options' => ['multiple' => true]])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('login')
            ->add('password')
            ->add('user', '', ['sortable' => 'user.username'])
            ->add('_action', 'actions', [
                'label' => $this->trans('Action'),
                'actions' => [
                    'delete' => [],
                ]
            ])
        ;
    }
}