<?php
/**
 * Created by PhpStorm.
 * User: rinat
 * Date: 09.04.15
 * Time: 17:10
 */

namespace Dicom\VitalImagesBundle\Services;

use Doctrine\ORM\EntityManager;

/**
 * Class VitalImagesManagement
 * @package Dicom\VitalImagesBundle\Services
 *
 * Doctrine\ORM\EntityRepository
 */
class VitalImagesManagement
{
    private $em;
    private $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repository = $this->em->getRepository('VitalImagesBundle:VitalImages');

    }

    public function getVitalImagesInfo($userId)
    {
        return $this->repository->findOneBy(['user_id' => $userId]);
    }
}