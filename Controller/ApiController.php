<?php

namespace Dicom\VitalImagesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiController
 * @package Dicom\VitalImagesBundle\Controller
 * @package Dicom\VitalImagesBundle\Services\VitalImagesManagement
 * @package Dicom\VitalImagesBundle\Entity\VitalImages
 */
class ApiController extends Controller
{
    public function getAction(Request $request)
    {
        if (!$userId = $request->attributes->get('id')) {
            return  new JsonResponse(["user" => []]);
        }

        $vitalImagesManagement = $this->get('vital_images.vital_images_management');
        $vitalUserInfo = $vitalImagesManagement->getVitalImagesInfo($userId);

        $user = $vitalUserInfo ? $vitalUserInfo->toArray() : [];

        return  new JsonResponse(["user" => $user]);
    }
}
